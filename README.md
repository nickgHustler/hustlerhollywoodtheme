Hustler Hollywood
===========================================================================================================================
This theme was designed for Hustler Hollywood.

Getting Started
===========================================================================================================================
These instructions will get you running the project on your local machine.


Prerequisites
===========================================================================================================================
- Node.js installed on your environement for development.


Installing
===========================================================================================================================
To get the project up and running, complete the following steps:
  1.  Download and install Node: https://nodejs.org/
  2. [Optional] Install Gulp globally: npm install gulp -g
  3.  Install project dependancies in "dev" folder using following command(s):
      cd dev
      npm install


Development
===========================================================================================================================
When developing components, you may want assets automatically compiled. To do this, in "dev" folder run the following command(s):
  gulp


Folder Structure
===========================================================================================================================
Sometimes it’s helpful to know what all these different files are for...

HH-CONSPIRE (Project Folder)
├── assets                                    
├── config
├── dev
|   ├── fonts
|   ├── images
|   ├── js
|   |   ├── functions                         # THEME'S scripts
|   |   |   ├── common.js
|   |   |   ├── legacy.js
|   |   ├── libs                              # PLUGIN's scripts
|   ├── sass
|   |   ├── components
|   |   ├── mixins
|   |   ├── partials
|   |   ├── sections
|   |   |   ├── common                        # styles of SECTION that appear on every page
|   |   |   |   ├── _footer.scss
|   |   |   |   ├── _header.scss
|   |   |   └── <section-name>.scss.liquid    # SECTION's styles
|   |   ├── templates
|   |   |   └── <template-name>.scss.liquid   # TEMPLATE's styles
|   |   ├── vendors                           # PLUGIN's styles
|   |   └── theme.scss.liquid                 
|   ├── sections
|   |   └── <section-name>.liquid             # SECTION's HTML structure and schema 
|   ├── Gulpfile.js                           # Configuration for Gulp tasks
|   └── package.json                          # Project manifest
├── layout
├── locales
├── sections
|   └── <section-name>.liquid                 # Concatenated from SECTION's styles, HTML structure and schema
├── snippets
└── templates
├─ .gitignore        # List of files and folders not tracked by Git
├─ LICENSE           # License information for this project
└─ README.md         # This file


Built With
===========================================================================================================================
This theme was built with:
  • Gulp:         https://gulpjs.com/
  • Theme Kit:    https://shopify.github.io/themekit/