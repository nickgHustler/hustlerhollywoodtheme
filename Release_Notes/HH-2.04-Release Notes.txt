HH-2.04 Release Notes:
 
1.    2 apps have been removed (one more in the process to be removed)
2.    Image Sitemap app re-activated now and images stared indexing back
3.    Collection speed improvement after SearchSpring optimization suggestions
4.    Nav Menu loading issue resolved and loads what is needed only per page
5.    SEO: Breadcrumbs for main collection like Sex Toys, Hustler Toys, New Arrivals have been fixed
6.    SEO: collections/products URL should be fixed now
7.    SEO: Helpful text is visible on Mobile in each collection now and both Mobile and Desktop top text is in the right place now
8.    SEO: Read More function doesn’t hide the rest of the text for crawlers now
9.    Main Brand collections fixed on Desktop: Womanizer, Lelo, We-Vibe.
10.   Look and Feel, UI/UX by Emily, especially noticeable on the Nav Menu.
11.   Top red banner on Mobile is centered now.
12.   A few not critical bugs from the last testing are fixed now.
13.   Google Map link on the Store Pages are showing Hustler Hollywood Store name instead of just address.
14.   Chat is back on the Homepage only.