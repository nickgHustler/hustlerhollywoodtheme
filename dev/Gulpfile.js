'use strict';
require('events').EventEmitter.defaultMaxListeners = 20;

const
    autoprefixer = require('gulp-autoprefixer'),
    babel = require('gulp-babel'),
    changed = require('gulp-changed'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    gcmq = require('gulp-group-css-media-queries'),
    gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    rename = require("gulp-rename"),
    replace = require('gulp-replace'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch');


function swallowError(error) {
    console.log(error.toString())
    this.emit('end')
}

/**
 * Common Stylesheet task
 */
gulp.task('css', function () {
    gulp.src('sass/**/theme.scss.liquid')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 3 versions', '> 5%', 'Explorer >= 10', 'Safari >= 8'],
            cascade: false
        }))
        .pipe(gcmq())
        .pipe(gulp.dest('.tmp/style/'))
        .pipe(rename('theme.scss.liquid'))
        //.pipe(cleanCSS({compatibility: 'ie8'}))
        // remove the extra set of quotations used for escaping the liquid string (we'll explain this in a sec)
        .pipe(replace('"{{', '{{'))
        .pipe(replace('}}"', '}}'))
        // save the file to the theme assets directory
        .pipe(gulp.dest('../assets/'));
});

/**
 * Section Stylesheet task
 */

//
const sectionsName = ['banner-for-couples', 'collection-heading', 'cta-group-2-for-couples', 'cta-group-2', 'cta-group-ad', 'cta-group', 'cta-sign-up', 'cta-sign-up-for-couples', 'grid-big-products', 'grid-featured-post', 'grid-img-txt', 'txt-img-bg', 'txt', 'product-spotlight', 'cta-gift-card'];

sectionsName.forEach(compileSectionStyle);

function compileSectionStyle(value, index, array) {
    gulp.task(`compile-css-${value}`, function (done) {
        gulp.src('sass/sections/' + value + '.scss.liquid')
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 3 versions', '> 5%', 'Explorer >= 10', 'Safari >= 8'],
                cascade: false
            }))
            .pipe(gcmq())
            .pipe(cleanCSS({compatibility: 'ie8'}))
            .pipe(replace('"{{', '{{'))
            .pipe(replace('}}"', '}}'))
            .pipe(gulp.dest('.tmp/style/sections/'))
            .on('end', done);
    });
    gulp.task(`merge-section-${value}`, [`compile-css-${value}`], function () {
        gulp.src(['sections/' + value + '.liquid', 'sass/sections/common/sections.css-opening.liquid', '.tmp/style/sections/' + value + '.scss.css', 'sass/sections/common/sections.css-closing.liquid'])
            .pipe(concat(value + '.liquid'))
            .pipe(gulp.dest('../sections/'))
    });
}


/**
 * Templates Stylesheet task
 */
const templatesName = ['account', 'blog', 'cart', 'collection', 'page', 'product', 'stores'];

templatesName.forEach(compileTemplateStyle);

function compileTemplateStyle (value, index, array) {
    gulp.task(`templates-css-${value}`, function () {
        gulp.src(`sass/templates/${value}.scss`)
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 3 versions', '> 5%', 'Explorer >= 10', 'Safari >= 8'],
                cascade: false
            }))
            .pipe(gcmq())
            .pipe(cleanCSS({compatibility: 'ie8'})).pipe(rename({
                extname: ".css"
              }))
            .pipe(gulp.dest('.tmp/style/'))
            // save the file to the theme assets directory
            .pipe(gulp.dest('../assets/'));
    });
}

/**
 * JavaScript task
 *
 * @type {string}
 */
const jsFiles = 'js/functions/*.js';
const jsDest = '../assets/';

gulp.task('js', function () {
    return gulp.src(jsFiles)
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('theme.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('theme.min.js'))
        //.pipe(uglify())
        .pipe(rename('theme.min.js.liquid'))
        .pipe(gulp.dest(jsDest));
});


/**
 * JavaScript Lib task
 *
 * @type {string}
 */
const jsLibFiles = 'js/libs/*.js';

gulp.task('vendor-js', function () {
    return gulp.src(jsLibFiles)
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});


/**
 * Images task
 */
//Imagemin Jpettran
gulp.task('compress-images', function () {
    gulp.src('images/**')
        .pipe(changed('../assets/')) // ignore unchanged files
        .on('error', swallowError)
        .pipe(imagemin({
            optimizationLevel: 6,
            progressive: true
        }))
        .pipe(gulp.dest('../assets/'));
});

/**
 * Fonts task
 */
gulp.task('fonts', function () {
    return watch('fonts/**', function () {
        gulp.src('fonts/**')
            .pipe(changed('../assets/')) // ignore unchanged files
            .pipe(gulp.dest('../assets/'));
    });
});

/**
 * Watch task
 */
gulp.task('watch', function () {
    gulp.watch([
        'sass/**/*.scss',
        '!sass/{sections,templates}/*'
    ], ['css']);
    sectionsName.forEach(watchSectionStyle);
    templatesName.forEach(watchTemplateStyle);
    gulp.watch('js/functions/*.js', ['js']);
    gulp.watch('js/libs/*.js', ['vendor-js']);
    //gulp.watch('js/modules/*.js', ['modules-js']);
    gulp.watch('images/*.{jpg,jpeg,png,gif,svg}', ['compress-images']);
    gulp.watch('fonts/*.{eot,svg,ttf,woff,woff2}', ['fonts']);
});

function watchSectionStyle(value, index, array) {
    gulp.watch(`sass/mixins/*.scss`, [`merge-section-${value}`]);
    gulp.watch(`sass/sections/${value}.scss.liquid`, [`merge-section-${value}`]);
    gulp.watch(`sections/${value}.liquid`, [`merge-section-${value}`]);
}

function watchTemplateStyle(value, index, array) {
    gulp.watch(`sass/templates/${value}.scss`, [`templates-css-${value}`]);
}


// Default Task
gulp.task('default', ['watch']);