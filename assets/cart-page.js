  $('.qty-field').on('change', function(e) {
    var newval = $(this).val();
    e.preventDefault();
    const qtyInput = $(this);
    const line_num = Number($(this).siblings('input[name="line_number"]').val());
    const quantity = Number($(this).val());
   
    const idx = line_num - 1;
    $(this).siblings('.updateicon').addClass('show');
    CartJS.updateItem(line_num, quantity, {}, {
      "success": function(data, textStatus, jqXHR) {
        if (!quantity) {
          window.location.href = "/cart"
        }
        qtyInput.val(data.items[idx].quantity)
        if (data.items[idx].quantity < quantity) {
          $("body").addClass("cart-error");
          $("body").find(".atc-state").addClass("atc-state--error");
          const responseTextDesc = "We don't have all the quantity you're trying to add. We've added as much as possible!"
          $("body").find(".atc-state").find(".atc-state__cnt").html("<h2 class=\"atc-state__desc\">" + responseTextDesc + "</h2>");

          setTimeout(function () {
              // Removing add to cart error status class on body
              $("body").removeClass("cart-error");
          }, 2500);
          setTimeout(function () {
              // Removing error message
              $("body").find(".atc-state").removeClass("atc-state--error");
          }, 50000);
          $(this).siblings('.updateicon').removeClass('show');
        } else {
          window.location.href = "/cart"
        }
      },
      "error": function(jqXHR, textStatus, errorThrown) {
        console.log('Error: ' + errorThrown + '!');
        console.log("text: ", textStatus)
      }
    });
  })

