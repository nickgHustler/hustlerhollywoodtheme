const $accordTrigger = $('.widget--blog-tags .accordion-trigger');
const $accordPanel = $('.widget--blog-tags .accordion-panel');
const accordion = function(){
    $(this).toggleClass('closed');
    $(this).next($accordPanel).slideToggle();
}
$accordTrigger.on('click', accordion);