//Quick View
$(document).ready(function() {
  $.getScript(
    "//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"
  ).done(function() {
    console.log('14th: jquery.fancybox.min.js LOADED');
    quickView();
    console.log('14th: quickview(); INIT');
  });
  
  // function IsEmail(email) {
  //   var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  //   if(!regex.test(email)) {
  //     return false;
  //   }else{
  //     return true;
  //   }
  // }
  
  // $(document).on('click', 'body #qv_BIS_trigger', function(e) {
  //   e.preventDefault();
  //   var checkuseremail = false;
  //   var checkuserphone = false;
  //   $('.qv-add-to-cart-response').text('').hide().removeClass('error success');
  //   var varid = $(this).attr('data-variant-id');
  //   var productid = $(this).attr('data-product-id');
  //   var useremail = $('body').find('#quick-view .viaemail').val();
  //   // var userphone = $('body').find('#quick-view .viaphone').val();
   
  //   if (useremail.length < 6) {
  //     $('.qv-add-to-cart-response').text('Please enter a valid email.').addClass('error').show();
  //     return false;
  //   }    

  //   if (varid.length < 2 || productid.length < 2) {
  //    $('.qv-add-to-cart-response').text('Error, could not add').show();
  //     return false;
  //   }
    
  //   if (useremail.length >= 1 && IsEmail(useremail)==true) {
  //     var passuseremail = useremail;
  //   } else if (useremail.length >= 1) {
  //      var passuseremail = 'null';
  //      $('.qv-add-to-cart-response').text('Please enter a valid email.').addClass('error').show();
  //      return false;
  //   }
     

  //   BIS.create(passuseremail, varid, productid).then(function(resp) { 
  //     if (resp.status == 'OK') {
  //       $('.qv-add-to-cart-response').text('Your notification has been successfully registered.').addClass('success').show();
  //       setTimeout(function() {
  //         $('.qv-add-to-cart-response').text('').removeClass('success').fadeOut();
  //       }, 5000);
  //     } else {
  //       $('.qv-add-to-cart-response').text('Error, could not add. Please recheck your email.').show();
  //     }
  //   });
  //   // BISPopover.show({variantId: varid});
  // });
});

function quickView() {
  $(".quick-view").click(function() {

    console.log('14th: .quick-view CLICKED!');
    
    if ($("#quick-view").length == 0) {
      console.log('14th: IF #quick-view length == 0 TRUE');
      $("body").append('<div id="quick-view"></div>');
    } else {
      console.log('14th: IF #quick-view length == 0 FALSE');
    }

    var product_handle = $(this).data("handle");
    console.log('14th: product_handle' + product_handle);
    $("#quick-view").addClass(product_handle);
    if (product_handle !== undefined) {
      console.log('14th: IF product_handle !== undefined TRUE');
      jQuery.getJSON("/products/" + product_handle + ".js", function(product) {
        var title = product.title;
        var type = product.type;
        var price = 0;
        var original_price = 0;
        var desc = product.description;
        var images = product.images;
        var media = product.media;
        var variants = product.variants;
        var options = product.options;
        var url = "/products/" + product_handle;
        $(".qv-product-title").text(title);
        //$('.qv-product-type').text(type);
        // $('.qv-product-description').html(desc);
        $(".view-product").attr("href", url);
        var imageCount = $(images).length;
        $(media).each(function(i, image) {
          if (i == imageCount - 1) {
            var image_embed =
              '<div><img data-lazy="' +
              image.src +
              '" class="slickimg" data-imageid="' +
              image.id +
              '"></div>';
            image_embed = image_embed
              .replace(".jpg", "_350x.jpg")
              .replace(".png", "_350x.png");
            $(".qv-product-images").append(image_embed);

            $(".qv-product-images")
              .slick({
                dots: true,
                arrows: true,
                respondTo: "min",
                useTransform: false,
                lazyLoad: 'ondemand',
                speed: 200,
              })
              .css("opacity", "1");
          } else {
            image_embed =
              '<div><img data-lazy="' +
              image.src +
              '" data-imageid="' +
              image.id +
              '" class="slickimg"></div>';
            image_embed = image_embed
              .replace(".jpg", "_350x.jpg")
              .replace(".png", "_350x.png");
            $(".qv-product-images").append(image_embed);
          }
        });
        $(options).each(function(i, option) {
          var opt = option.name;
         
          
          var selectClass = ".option." + opt.toLowerCase();
          $(".qv-product-options").append(
            '<div class="option-selection-' +
              opt.toLowerCase() +
              '"><span class="option">' +
              opt +
              '</span><select class="option-' +
              i +
              " option " +
              opt.toLowerCase() +
              '"></select></div>'
          );
           var optcount = option.values.length;
          $(option.values).each(function(i, value) {
            $(".option." + opt.toLowerCase()).append(
              '<option value="' + value + '">' + value + "</option>"
            );
          });
          if (optcount < 2) {
           $("select.option-"+i).addClass("onlyone"); 
          }
        });

        $(product.variants).each(function(i, v) {
          if (v.inventory_quantity == 0) {
            $(".qv-add-button")
              .prop("disabled", true)
              .val("Sold Out");
            // $('.qv-add-to-cart').hide();
            $(".qv-product-price")
              .text("Sold Out")
              .show();
            return true;
          } else {
            price = parseFloat(v.price / 100).toFixed(2);
            original_price = parseFloat(v.compare_at_price / 100).toFixed(2);
            $(".qv-product-price").text("$" + price);
            if (original_price > 0) {
              $(".qv-product-original-price")
                .text("$" + original_price)
                .show();
            } else {
              $(".qv-product-original-price").hide();
            }
            $("select.option-0").val(v.option1);
            $("select.option-1").val(v.option2);
            $("select.option-2").val(v.option3);
            return false;
          }
        });

        $("body")
          .find("#quick-view select.option-0")
          .trigger("change");
        window.BOLD.common.Shopify.saveProduct(product.handle, product.id);
        for (let v of product.variants) {
          window.BOLD.common.Shopify.saveVariant(v.id, {
            product_id: product.id,
            price: v.price,
          });
        }
        window.BOLD.common.eventEmitter.emit("BOLD_COMMON_cart_loaded");
      });
    } else {
      console.log('14th: IF product_handle !== undefined FALSE');
    } // check at valid product_handle

    $(document).on("change", "#quick-view select", function() {
      var selectedOptions = "";
      $("#quick-view select").each(function(i) {
        if (selectedOptions == "") {
          selectedOptions = $(this).val();
        } else {
          selectedOptions = selectedOptions + " / " + $(this).val();
        }
      });
      if (product_handle !== undefined) {
        jQuery.getJSON("/products/" + product_handle + ".js", function(product) {
         
          $(product.variants).each(function(i, v) {
            if (v.title == selectedOptions) {
              var price = parseFloat(v.price / 100).toFixed(2);
              var original_price = parseFloat(v.compare_at_price / 100).toFixed(2);
              var v_qty = v.inventory_quantity;
              var v_inv = v.inventory_management;
              var v_image = v.featured_image.id;
              var v_imagesrcraw = v.featured_image.src;
              var v_imagesrcslice = v_imagesrcraw
                .replace(".jpg", "_350x.jpg")
                .replace(".png", "_350x.png");
              var v_imagesrc = v_imagesrcslice.split("?v=", 1);
              var slickindex = $(".qv-product");
              var vallower = selectedOptions.toLowerCase();
              imgindex = ""; //reset
              // $("#qv_BIS_trigger").attr("data-variant-id", v.id).attr("data-product-id", product.id);
              $(".slick-track img.slickimg").each(function() {
                var thissrc = $(this).attr("data-src");
                if (thissrc.includes(v_imagesrc)) {
                  imgindex = $(this)
                    .parent(".slick-slide")
                    .attr("data-slick-index");
                }
              });
              if (imgindex != "" && isinitial == false) {
                var totalSlides = $(".qv-product-images .slick-dots li").length;
                var currentSlide = $(".qv-product-images").slick(
                  "slickCurrentSlide"
                );
                if (imgindex >= totalSlides) {
                  imgindex = imgindex - totalSlides;
                }
                if (imgindex !== currentSlide) {
                  $(".qv-product-images").slick("slickGoTo", imgindex, false);
                }
              } else if (isinitial == true) {
                isinitial = false;
              }
              $(".qv-product-price").text("$" + price);
              $(".qv-product-original-price").text("$" + original_price);
            
              if (v_inv == null) {
                $(".qv-add-button")
                  .prop("disabled", false)
                  .val("Add to Cart");
                $('.notify-qv-availability').show();
          
              } else {
                if (v.inventory_quantity < 1) {
                  $(".qv-add-button")
                    .prop("disabled", true)
                    .val("Sold Out");
                  $('.notify-qv-availability').show();
             
                } else {
                  $(".qv-add-button")
                    .prop("disabled", false)
                    .val("Add to Cart");
                  $('.notify-qv-availability').hide();
                }
              }
            }
          });
        });
      } // check at least valid product_handle
    });
    $.fancybox({
      href: "#quick-view",
      maxWidth: 700,
      // maxHeight: 450,
      fitToView: true,
      width: "100%",
      height: "70%",
      autoSize: false,
      closeClick: false,
      openEffect: "none",
      closeEffect: "none",
      beforeLoad: function() {
        var product_handle = $("#quick-view").attr("class");
        isinitial = true;
        $(document).on("click", ".qv-add-button", function() {
          var qty = $(".qv-quantity").val();
          var selectedOptions = "";
          var var_id = "";
          $("#quick-view select.option").each(function(i) {
            if (selectedOptions == "") {
              selectedOptions = $(this).val();
            } else {
              selectedOptions = selectedOptions + " / " + $(this).val();
            }
          });
          if (product_handle !== undefined) {
            jQuery.getJSON("/products/" + product_handle + ".js", function(
              product
            ) {
              $(product.variants).each(function(i, v) {
                if (v.title == selectedOptions) {
                  var_id = v.id;

                  processCart();
                  $("#quick-view .qv-add-button").unbind("click");
                  product_handle = undefined;
                }
              });
            });
          } // check valid product_handle
          function processCart() {
            jQuery
              .post(
                "/cart/add",
                {
                  quantity: qty,
                  id: var_id,
                },
                null,
                "json"
              )
              .done(function() {
                $.fancybox.close();
                //$('.qv-add-to-cart-response').addClass('success').html('<span>' + $('.qv-product-title').text() + ' has been added to your cart. <a href="/cart">Click here to view your cart.</a>');
                app.cart.openmini();
                app.cart.refresh();
              })
              .fail(function($xhr) {
                var data = $xhr.responseJSON;
                $(".qv-add-to-cart-response")
                  .addClass("error")
                  .html("<span><b>ERROR: </b>" + data.description);
              });
          }
        });

        $(".fancybox-wrap").css("overflow", "hidden !important");
      },
      afterShow: function() {
        $("#quick-view")
          .hide()
          .html(content)
          .css("opacity", "1")
          .fadeIn(function() {
            $(".qv-product-images").addClass("loaded");
          });
      },
      afterClose: function() {
        $("#quick-view")
          .removeClass()
          .empty();
        product_handle = undefined;
        product = undefined;
      },
      onClosed: function() {
        $("#quick-view").empty();
      },
    });
  });
}



$(window).resize(function() {
  if ($("#quick-view").is(":visible")) {
    $(".qv-product-images").slick("setPosition");
  }
});

