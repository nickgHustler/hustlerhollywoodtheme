const product_app = {};
product_app.cache = function () {
    product_app.$window = $(window),
    product_app.$headerMiddleBar = $('.header .main-menu-bar'),
    product_app.$slider = $('.product--single .product-photo-container'),
    product_app.$sliderNav = $('.product--single .product-photo-thumbs'),
    product_app.$accordTrigger = $('.product--single .accordion-trigger'),
    product_app.$accordPanel = $('.product--single .accordion-panel'),
    product_app.$stickyBar = $('.product--single .mobile-sticky-bar'),
    product_app.$stickyWrap = $('.product--single .sticky-wrap'),
    product_app.$videoTrigger = $('.product--single .video-media .play-video'),
    product_app.$customChat = $('#custom_fc_button')
    ;
};
product_app.init = function () {
    product_app.cache();
    product_app.imageSliders();
    product_app.accordion();
    //product_app.fancybox();
    //product_app.stickyCart();
    product_app.variantSelect();
    product_app.quantityChange();
    product_app.greyOutUnavailableVars();
    product_app.videoPlay();
};
product_app.load = function () {
  product_app.stickyCart();
};
product_app.imageSliders = function(){
    product_app.$slider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        asNavFor: '.product--single .product-photo-thumbs',
        speed: 200,
        infinite: false,
        draggable: false,
        responsive: [
            {
              breakpoint: 767,
              settings: {
                dots: true,
                draggable: true
              }
            }
          ]
    });


    product_app.$sliderNav.slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        prevArrow: '<a class="icon-angle-arrow-left slick-arrow slick-prev"></a>',
        nextArrow: '<a class="icon-angle-arrow-right slick-arrow slick-next"></a>',
        centerMode: false,
        focusOnSelect: true,
        vertical: true,
        verticalSwiping: true,
        responsive: true,
        draggable: true,
        asNavFor: '.product--single .product-photo-container',
        infinite: false
    });
    product_app.$sliderNav.css("visibility", "visible")
    // $('.product-media-holder.video-media').css('display', 'block')
};


product_app.fancybox = function(){
    $('.product-main-gallery [data-fancybox="gallery"]').fancybox({
        backFocus: false,
        hash     : false,
        wheel: false,
      arrows: true,
      idleTime: false
    })
};
product_app.accordion = function(){
    product_app.$accordTrigger.on('click',function(){
        $(this).toggleClass('closed');
        $(this).next(product_app.$accordPanel).slideToggle();
    });
};
product_app.videoPlay = function(){
  product_app.$videoTrigger.on('click', function () {
    var $parent = $(this).parent('.video-media'),
      $thumb = $parent.find('.video-thumb'),
      $video = $parent.find('.yt-video');
      $parent.addClass('show-vdo');
      $(this).fadeOut();
      $thumb.fadeOut();
      if ($video[0].src.indexOf('autoplay=0') != -1) {
        $video[0].src.replace('autoplay=0', 'autoplay=1');
      } else {
        $video[0].src += "&autoplay=1";
      }
  });
};
stickyOff = false;
product_app.stickyCart = function(){
  var stikcyOffset =  $('.product--single .mobile-sticky-bar').offset(),
  elemoffsetTop = stikcyOffset.top,
  headerHeight = product_app.$headerMiddleBar.height(),
  stickyElemHeight = product_app.$stickyWrap.height() + 50,
  reqValue = elemoffsetTop + headerHeight + stickyElemHeight
  ;
  if (product_app.$window.width() < 768) {
    product_app.$stickyBar.css({
        "height": stickyElemHeight
    });
    product_app.$stickyWrap.addClass('scene');

    // product_app.$window.scroll(function(){
    //   if($(this).scrollTop() > reqValue){
    //     if ( !stickyOff) {
    //       product_app.$stickyBar.addClass('sticky');
    //       var imgsrc = $('.product_photo-item.slick-current.slick-active').find('.product-thumb').attr('data-original-image-80x');
    //       $('.mobile-sticky-img').find('img').attr('src', imgsrc);
    //       product_app.$customChat.css('bottom', stickyElemHeight+'px');
    //     }
    //   } else {
    //     product_app.$stickyBar.removeClass('sticky').removeClass('expanded');
    //     product_app.$customChat.css('bottom', '65px');
    //   }
    // });

    /* change the above code to use passive Event Listener for performance boost */
    document.addEventListener('scroll', (evt) => {
      if(window.scrollY > reqValue){
        if ( !stickyOff ) {
          product_app.$stickyBar.addClass('sticky');
          if ( $('.mobile-sticky-img').find('img').attr('src').indexOf('&placeholder_image') != -1 ) {
            var imgsrc = $('.product_photo-item.slick-current.slick-active').find('.product-thumb').attr('data-original-image-80x');
            $('.mobile-sticky-img').find('img').attr('src', imgsrc); 
          }
          product_app.$customChat.css('bottom', stickyElemHeight+'px');
        }
      } else {
        product_app.$stickyBar.removeClass('sticky').removeClass('expanded');
        product_app.$customChat.css('bottom', '65px');
      }
    }, { passive: true });
  }
}

$('.expand-sticky')[0].addEventListener('click', function() {
  
  if (product_app.$stickyBar.is('.expanded')) {
    product_app.$stickyBar.removeClass('expanded');
  } else {
    product_app.$stickyBar.addClass('expanded');
  }

}, { passive: true });

$('.expand-sticky')[0].addEventListener('touch', function() {
  
  if (product_app.$stickyBar.is('.expanded')) {
    product_app.$stickyBar.removeClass('expanded');
  } else {
    product_app.$stickyBar.addClass('expanded');
  }
  
}, { passive: true });

// $('.expand-sticky').on('click touch', function() {
//   if (product_app.$stickyBar.is('.expanded')) {
//     product_app.$stickyBar.removeClass('expanded');
//   } else {
//     product_app.$stickyBar.addClass('expanded');
//   }
// });


product_app.quantityChange = function(){
  const invCheck = (curr, thres) => {
    const currNum = Number(curr);
    if (currNum > thres) {
      $('#product-add .styled-qty-input-wrapper #quantity').val(thres);
    }
  }
  $('#product-add .styled-qty-input-wrapper #quantity').keyup(function() {
    if (Number($(this).val()) > product_app.curr_inv_quantity) {
      $(this).val(product_app.curr_inv_quantity);
    }
    invCheck($('#product-add .styled-qty-input-wrapper #quantity').val(), product_app.curr_inv_quantity);
    //invCheck($(this).val(), product_app.curr_inv_quantity);
  })
  $('.qtyplus').click(function() {
    invCheck($('#product-add .styled-qty-input-wrapper #quantity').val(), product_app.curr_inv_quantity);
  })
  $('.qtyminus').click(function() {
    invCheck($('#product-add .styled-qty-input-wrapper #quantity').val(), product_app.curr_inv_quantity);
  })
};

product_app.prevImageID = '';
product_app.greyOutUnavailableVars = function(){
  if (!$('#DNR').val()) return;
  const singleOptionSelectors = document.querySelectorAll(".single-option-selector");
  const singleOptionSelectorOptions = document.querySelectorAll(".single-option-selector option")
  const disabledOptions = document.querySelectorAll('#product-select option[disabled = "disabled"]')
 /* if (singleOptionSelectors.length === 1) {
    const singleOptionValues = Array.from(singleOptionSelectorOptions).map(function(option) { return option.value });
    const disabledVariants = Array.from(disabledOptions).map(function(option) { return option.innerHTML.split(" - ")[0] });
    singleOptionValues.forEach(function(optionVal, i) {
      disabledVariants.forEach(function(disabledVar, j) {
        if (!disabledVar.includes(optionVal)) return;
        const selectOption = document.querySelector(`#product-select-option-${i} option[value="${disabledVar}"]`)
        selectOption.setAttribute("disabled", "disabled");
      })
    })
  } */
 // if (singleOptionSelectors.length === 2) {

  //}
}
product_app.variantSelect = function(){
  var selectCallback = function(variant, selector) {
    if (variant) {

      var variantID = variant.id; // New image object.
      $('.product--single .js-variant-id').val(variantID);
      // $('.product--single .product-photo-thumbs .product-photo-thumb[data-variant-id='+ variantID +']').trigger('click');

      if (!(variant.featured_image == null)) {
        // Check if previous variant's image id equal to selected variant's image id
        if(product_app.prevImageID !== variant.featured_image.id) {
          product_app.prevImageID = variant.featured_image.id;
          var currentSlide = $('.product--single .product-photo-thumbs').slick('slickCurrentSlide'),
              reqSlide = $('.product--single .product-photo-thumbs .product-photo-thumb[data-image-id='+ variant.featured_image.id +']').index();
          if(currentSlide !== reqSlide) {
            product_app.$sliderNav.slick('slickGoTo', reqSlide);
          }
        }
      }


      // Selected a valid variant that is available.
      if (variant.available) {
        product_app.curr_inv_quantity = variant.inventory_quantity;
        // Enabling add to cart button.
        $('.product--single  .btn-addcart').removeClass('disabled').prop('disabled', false).html( "Add To Cart");

        // If item is backordered yet can still be ordered, we'll show special message.
        if (variant.inventory_management && variant.inventory_quantity <= 0) {
          $('.product--single  #selected-variant').html( $('.product--single').data('title')  + ' - ' + variant.title);
          $('.product--single #backorder').removeClass("hidden");
        } else {
          $('.product--single #backorder').addClass("hidden");
        }

      } else {
        // Variant is sold out.
        $('.product--single #backorder').addClass('hidden');
        $('.product--single .btn-addcart').html("Sold Out").addClass('disabled').prop('disabled', true);
      }

      // Whether the variant is in stock or not, we can update the price and compare at price.
      if ( variant.compare_at_price > variant.price ) {

            var comparePrice = variant.compare_at_price,
                normalPrice = variant.price,
                savings = comparePrice - normalPrice,
                savingsPercentage = 100 - (100/comparePrice)*normalPrice,
                savingsPercentageDecimal = savingsPercentage.toFixed(1)
            ;

        $('.product--single #product-price').html('<div class="sale-wrapper"><div class="col-auto"><span class="sale-info">super sale</span><span class="product-price on-sale">'+ Shopify.formatMoney(variant.price, "") +'</span></div><div class="col-auto">'+'<div class="product-compare-price">Was '+Shopify.formatMoney(variant.compare_at_price, "")+ '</div><div class="price-savings">Save '+Shopify.formatMoney(savings, "")+'<span class="percentage-savings"> ('+ savingsPercentageDecimal +'%) </span></div></div></div>');
      } else {
        $('.product--single #product-price').html('<span class="product-price">'+ Shopify.formatMoney(variant.price, "") + '</span>' );
      }

      // show when product is eligible for free shipping
      if ( variant.price/100 > 79  ) {
        $('.product--single .free-shipping-info').removeClass('hidden');
      }
      $('.rewards-text .reward-pts').html(Math.round(variant.price / 100))
    } else {
      // variant doesn't exist.
      $('.product--single #product-price').empty();
      $('.product--single #backorder').addClass('hidden');
      $('.product--single .btn-addcart').html( "Unavailable").addClass('disabled').prop('disabled', true);

    }

    var mobileStickyImgURL = $('.mobile-sticky-img').find('img').attr('src');
    var mobileStickyImgNewURL = mobileStickyImgURL + '&placeholder_image=true'
    $('.mobile-sticky-img').find('img').attr('src', mobileStickyImgNewURL);

    product_app.greyOutUnavailableVars();
  };
  if($('#product-select').length) {
    var optionSelectors =  new Shopify.OptionSelectors('product-select', {
      product: $('.product--single').data('product'),
      onVariantSelected: selectCallback,
      enableHistoryState: false
    });
    // $(document).on('click','.product--single [data-change-variant]',function(){
    //   optionSelectors.selectVariant($(this).data('changeVariant'));
    // });
    product_app.$sliderNav.on('afterChange', function(event, slick, currentSlide, nextSlide){
      // Check if previous variant's image id equal to current slide's image id
      if(product_app.prevImageID !== $('.product--single .product-photo-thumbs .product-photo-thumb').eq(currentSlide).data('image-id')) {
        var currentVariant = $('.product--single .product-photo-thumbs .product-photo-thumb').eq(currentSlide).data('variant-id');
        optionSelectors.selectVariant(currentVariant);
      }
    });
  }
}
$(document).ready(function () {
    product_app.init();
});
$(window).on('load',function () {
  product_app.load();
});

$('.mobile-sticky-close').on('click touch', function() {
$('.mobile-sticky-bar').removeClass('sticky');
      stickyOff = true;
});

$('.product--faq-question').on('click touch', function(){
    if ($(this).is('.showing')) {
         $(this).removeClass('showing');
    $(this).next('.product--faq-answer').slideUp();
        } else {
         $(this).addClass('showing');
  $(this).next('.product--faq-answer').slideDown();
        }
  });

  $('#quantity').on('keyup',function() {
    if ($(this).val() == '0') {
      $(this).val(1);
    }
  });

/** Mobile Re-arranging **/
  function rearrangeMobile() {
    // if ($('.product-media-holder.video-media').length) {
    //  if ($(window).width() < 768 ) {

    //   $('.product-media-holder.video-media').appendTo('.product__details');
    // } else {
    //   $('.product-media-holder.video-media').appendTo('.product__photos');
    // }
    // }

    if ($('.youtube-player.video-media').length) {
      if ($(window).width() < 768 ) {
        $('.youtube-player.video-media').appendTo('.product__details');
      } else {
        $('.youtube-player.video-media').appendTo('.product__photos');
      }
    }

  }

  $(function() {
   rearrangeMobile();

  });
 $(window).resize(function() {
    rearrangeMobile();
  });

  /** Age Gate Items **/
    $(function() {
      alreadyagechecked = false;
      $('.checkage-first').on('click', function() {
        if ($(this).hasClass('disabled')) {
            return false;
        }
          if (alreadyagechecked) {
            $.modal.close();
          $('#product-add').find('.product__action.product__action--add-to-cart.js-add-to-cart').trigger('click');
            return false;
          }

           $("#agegate-modal").modal({
  escapeClose: false,
  clickClose: true,
  showClose: false
});

      });

      $('#btn-checkage').on('click', function() {

         $('.modal-error-text').hide();
        var agemonth = $('#agecheck_month').val();
        var ageday = $('#agecheck_day').val();
        var ageyear = parseInt($('#agecheck_year').val());
        var age = 21;

        if (parseInt(agemonth) < 13) {


        } else {
          $('.modal-error-text').show().find('span').text('Please enter a valid month.');
          return false;
        }



         if (parseInt(ageday) < 32) {


        } else {
          $('.modal-error-text').show().find('span').text('Please enter a valid day.');
          return false;
        }

        if (ageyear >= 1900 ) {

        } else {
          $('.modal-error-text').show().find('span').text('Please enter a valid year.');
          return false;
        }

        if (agemonth == 2 && (ageday == 30 || ageday == 31)) {
            $('.modal-error-text').show().find('span').text('Please enter a valid date.');
          return false;
        } else if (agemonth == 2 && ageday == 29 && ageyear % 4 != 0) {
          $('.modal-error-text').show().find('span').text('Please enter a valid date.');
          return false;

        }

        var mydate = new Date();
        mydate.setFullYear(ageyear, agemonth - 1, ageday);
        var currdate = new Date();
        currdate.setFullYear(currdate.getFullYear() - age);

        if ((currdate - mydate) < 0) {
        $('.modal-error-text').show().find('span').text('Sorry you must be at least 21 to purchase this product.');
        return false;
        } else {
          alreadyagechecked = true;
          if (alreadyagechecked) {
            $.modal.close();
          $('#product-add').find('.product__action.product__action--add-to-cart.js-add-to-cart').trigger('click');
            return false;
          }

        }


      });
  });


  $(function() {
    if ($('#product-select-option-0').length) {
 $('#product-select-option-0').each(function() {
      var length = $('#product-select-option-0 > option').length;
       const optName = $('#product-select').attr(`data-opt0`)
     if (length < 2) {
       $(this).addClass('onlyone');
       $(this).parents('.selector-wrapper').addClass('onlyone');

     }
     if (!$('label[for="product-select-option-0"]').length) {
       $(this).parents('.selector-wrapper').prepend(`<label for="product-select-option-0">${optName}</label>`)
     }

    });
    }
    if ($('#product-select-option-1').length) {
 $('#product-select-option-1').each(function() {
        var length = $('#product-select-option-1 > option').length;
        const optName = $('#product-select').attr(`data-opt1`)
        if (length < 2) {
           $(this).addClass('onlyone');
          $(this).parents('.selector-wrapper').addClass('onlyone');
        }
       if (!$('label[for="product-select-option-1"]').length) {
         $(this).parents('.selector-wrapper').prepend(`<label for="product-select-option-1">${optName}</label>`)
       }
    });
    }
    if ($('#product-select-option-2').length) {
 $('#product-select-option-2').each(function() {
      var length = $('#product-select-option-2 > option').length;
     const optName = $('#product-select').attr(`data-opt2`)
      if (length < 2) {
         $(this).addClass('onlyone');
        $(this).parents('.selector-wrapper').addClass('onlyone');
       }
       if (!$('label[for="product-select-option-2"]').length) {
         $(this).parents('.selector-wrapper').prepend(`<label for="product-select-option-2">${optName}</label>`)
       }
    });
    }

  });
