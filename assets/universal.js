var sa_uni = sa_uni || [];
sa_uni.push(["sa_pg", sa_page]);
(function() {
	function sa_async_load() {
		var sa = document.createElement("script");
		sa.type = "text/javascript";
		sa.async = true;
		sa.src = "//cdn.socialannex.com/partner/8990111/universal.js";
		var sax = document.getElementsByTagName("script")[0];
		sax.parentNode.insertBefore(sa, sax);
	}
	if (window.attachEvent) {
		window.attachEvent("onload", sa_async_load);
	} else {
		window.addEventListener("load", sa_async_load, false);
	}
})();
