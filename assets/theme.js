"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var app = {
    currentScrollTop: 0
};
var resizeTimer = void 0; // Set resizeTimer to empty so it resets on page load
window.hh_conspire = function (window, document, $) {
    app.cache = function () {
        app.$window = $(window);
        app.$document = $(document);
        app.$body = $("body");
        app.$atcState = app.$body.find(".atc-state");
        app.$atcCnt = app.$atcState.find(".atc-state__cnt");
        app.$window = $(window);
        app.scrollUp = false;
    };

    app.init = function () {
        app.cache();

        app.winPos = app.$window.scrollTop();
        app.winLastPos = app.winPos;
        app.winWidth = $(window).outerWidth();

        app.headerSearchReset();

        app.initReadMore();

        var miniCart = new app.cart.mini();
        app.cart.refresh();
        app.detectDevices();
        app.mobileMenuToggle();
        // Custom Select
        if ($(".custom-select").length) {
            app.addCustomSelect();
            app.triggerCloseAll();
        }

        // Bookmark This
        if ($(".js-bookmark-this").length) {
            app.$body.on("click", ".js-bookmark-this", function (e) {
                app.bookmarkThis(this, e, window.location.href, document.title);
            });
        }

        // Slick Slider
        app.slickSliders();

        // Facet Dropdown
        if ($(".facet--dropdown").length) {
            app.facetDropdown();
            app.triggerCloseAllFacetDropdown();
        }

        // Header Scroll Effects
        app.initHeaderScroll();

        // Default Collection Scroll Effects
        app.initCategoryLanding();

        // Cart
        app.cart.init();

        // Blog Tag Dropdown
        if (app.$body.hasClass("template-article") || app.$body.hasClass("template-blog")) {
            app.blog.init();
        }
    };

    /**
     * Internal Functions
     */
    app.findElPos = function ($el) {
        return $el.offset().top;
    };
    app.isMobile = {
        Android: function Android() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function BlackBerry() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function iOS() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function Opera() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function Windows() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function any() {
            return app.isMobile.Android() || app.isMobile.BlackBerry() || app.isMobile.iOS() || app.isMobile.Opera() || app.isMobile.Windows();
        }
    };
    app.detectDevices = function () {
        if (app.isMobile.any()) {
            $("body").toggleClass("is-handheld");
        }
    };
    app.bookmarkThis = function (__this, e, bookmarkURL, bookmarkTitle) {
        try {
            // Internet Explorer solution
            eval("window.external.AddFa-vorite(bookmarkURL, bookmarkTitle)".replace(/-/g, ""));
        } catch (e) {
            try {
                // Mozilla Firefox solution
                window.sidebar.addPanel(bookmarkTitle, bookmarkURL, "");
            } catch (e) {
                // Opera solution
                if ((typeof opera === "undefined" ? "undefined" : _typeof(opera)) == "object") {
                    __this.rel = "sidebar";
                    __this.title = bookmarkTitle;
                    __this.url = bookmarkURL;
                    return true;
                } else {
                    // The rest browsers (i.e Chrome, Safari)
                    alert("Press " + (navigator.userAgent.toLowerCase().indexOf("mac") != -1 ? "Cmd" : "Ctrl") + "+D to bookmark this page.");
                }
            }
        }

        return false;
    };
    /**
     * End Internal Functions
     */

    /**
     * Header Search Reset
     */
    app.headerSearchReset = function () {
        var $form = $("header.header .form--search");
        $form.each(function () {
            $(this).on("focus keyup input change keypress", 'input[type="search"]', function () {
                var $reset = $(this).siblings('button[type="reset"]');
                if ($(this).val()) {
                    $reset.addClass("visible");
                } else {
                    $reset.removeClass("visible");
                }
            });
            $(this).on("blur", 'input[type="search"]', function () {
                var $reset = $(this).siblings('button[type="reset"]');
                $reset.removeClass("visible");
            });
        });
        $form.on("mousedown", 'button[type="reset"]', function (event) {
            event.preventDefault();
            $(this).siblings('input[type="search"]').val("").trigger("keyup change");
            $(this).removeClass("visible");
        });
    };

    /**
     *  Read More Function
     */
    app.initReadMore = function () {
        if ($(".js-desc .js-readmore").length) {
            $(".js-readmore, .js-readless").click(function (event) {
                event.preventDefault();
                var $currentDesc = $(this).closest(".js-desc");
                var $descriptionFull = $currentDesc.find(".js-desc-full");
                $descriptionFull.toggle();
                var $descriptionShort = $currentDesc.find(".js-desc-short");
                $descriptionShort.toggle();
            });
        }
    };
    /**
     *  End Read More Function
     */

    /**
     * Slick Slider
     */
    app.slickSliders = function () {
        // Horizontal Slider
        if ($(".slider--horizontal").length) {
            $(".slider--horizontal").each(function () {
                var $this = $(this),
                    infinite = $(this).data("infinite") ? $(this).data("infinite") : false;
                // here variable "a" denotes number of slidesToShow
                var a = new Array();
                a[0] = $this.data("slides-to-show-xs") ? $this.data("slides-to-show-xs") : 1;
                a[5] = $this.data("slides-to-show") ? $this.data("slides-to-show") : 5;

                // Arithmetic Sequence
                function findNthTermArithmeticSeq(a1, aN, n) {
                    var d = (aN - a1) / (n - 1);
                    for (var i = 1; i < n - 1; i++) {
                        a[i] = Math.ceil(a1 + i * d);
                    }
                }

                // Find terms in arithmetic sequence
                findNthTermArithmeticSeq(a[0], a[5], 6);

                // Horizontal Slider Initialized
                $this.slick({
                    dots: false,
                    infinite: infinite,
                    speed: 300,
                    slidesToShow: a[5],
                    slidesToScroll: 1,
                    swipeToSlide: true,
                    prevArrow: '<a class="icon-angle-arrow-left slick-arrow slick-prev"></a>',
                    nextArrow: '<a class="icon-angle-arrow-right slick-arrow slick-next"></a>',
                    responsive: [{
                        breakpoint: 1920,
                        settings: {
                            slidesToShow: a[4],
                            slidesToScroll: 1
                        }
                    }, {
                        breakpoint: 1262,
                        settings: {
                            slidesToShow: a[3],
                            slidesToScroll: 1
                        }
                    }, {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: a[2],
                            slidesToScroll: 1
                        }
                    }, {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: a[1],
                            slidesToScroll: a[1],
                            arrow: false
                        }
                    }, {
                        breakpoint: 577,
                        settings: {
                            slidesToShow: a[0],
                            slidesToScroll: a[0],
                            arrow: false
                        }
                        // You can unslick at a given breakpoint now by adding:
                        // settings: "unslick"
                        // instead of a settings object
                    }]
                });
            });
        }
    };
    /**
     * End Slick Slider
     */

    /**
     * Custom Select
     */
    app.addCustomSelect = function () {
        var x, i, j, selElmnt, a, b, c;
        /*look for any elements with the class "custom-select":*/
        x = document.querySelectorAll(".custom-select,.selector-wrapper");
        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            /*for each element, create a new DIV that will act as the selected item:*/
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);
            /*for each element, create a new DIV that will contain the option list:*/
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 0; j < selElmnt.length; j++) {
                /*for each option in the original select element,
                 create a new DIV that will act as an option item:*/
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function (e) {
                    /*when an item is clicked, update the original select box,
                     and the selected item:*/
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.value = s.getElementsByTagName("option")[i].value;
                            s.getElementsByTagName("option")[i].click();
                            var event = new Event("change");
                            s.dispatchEvent(event);
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function (e) {
                /*when the select box is clicked, close any other select boxes,
                 and open/close the current select box:*/
                e.stopPropagation();
                app.closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
                this.parentNode.classList.toggle("custom-select--active");
                this.closest(".facet--select").classList.toggle("facet--open");
                // Close other facet boxes when grouped with facets
                if ($(".facet--dropdown").length) {
                    app.closeAllFacetDropdown(this.closest(".facet--select"));
                }
            });
        }
    };
    app.updateCustomSelect = function (selectWrapper) {
        var x, i, j, selElmnt, a, b, c;
        /*look for any elements with the class "custom-select":*/
        x = selectWrapper;
        selElmnt = x.getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        if (!(x.getElementsByClassName("select-selected").length > 0)) {
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x.appendChild(a);
        }
        /*for each element, create a new DIV that will contain the option list:*/
        b = x.getElementsByClassName("select-items select-hide")[0];
        while (b.firstChild) {
            b.removeChild(b.firstChild);
        }

        for (j = 0; j < selElmnt.length; j++) {
            /*for each option in the original select element,
             create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                 and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.value = s.getElementsByTagName("option")[i].value;
                        var event = new Event("change");
                        s.dispatchEvent(event);
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
    };
    app.closeAllSelect = function (elmnt) {
        /*a function that will close all select boxes in the document,
         except the current select box:*/
        var x,
            y,
            i,
            arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i);
            } else {
                y[i].classList.remove("select-arrow-active");
                y[i].parentNode.classList.remove("custom-select--active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    };
    app.triggerCloseAll = function () {
        /*if the user clicks anywhere outside the select box,
        then close all select boxes:*/
        document.addEventListener("click", app.closeAllSelect);
    };
    /**
     * End Custom Select
     */

    app.mobileMenuToggle = function () {
        var $mobileMenu = $(".nav--primary-mobile"),
            $mobileDropdownSign = $mobileMenu.find(".dropdown__toggle"),
            $mobileDropdownTrigger = $mobileMenu.find(".dropdown a.nav__link"),
            $mobileMenuTrigger = $(".nav-trigger"),
            $mobileMenuWrap = $(".nav-group--mobile");



    };

    /**
     * Facet Dropdown
     */
    app.subFacetDropdown = function () {
        $("body").on("click", ".sub-facet--expandown .expandown-btn", function (event) {
            // its click event doesn't bubble up to the document
            event.stopPropagation();
            var $targetEl = $(this).closest(".sub-facet--expandown");

            $targetEl.toggleClass("sub-facet--open").find(".sub-facet__cnt").toggleClass("sub-facet__cnt--show").slideToggle();
            $targetEl.closest(".sub-facets-group").find(".sub-facet--expandown").not($targetEl).removeClass("sub-facet--open").find(".sub-facet__cnt").removeClass("sub-facet__cnt--show").slideUp();
        });
    };
    app.facetDropdown = function () {
        $("body").on("click", ".facet--dropdown .dropdown-btn", function (event) {
            // its click event doesn't bubble up to the document
            event.stopPropagation();
            var $targetEl = $(this).closest(".facet--dropdown");

            $targetEl.toggleClass("facet--open").find(".facet__cnt").toggleClass("facet__cnt--show");
            $targetEl.closest(".js-facets").find(".facet--dropdown").not($targetEl).removeClass("facet--open").find(".facet__cnt").removeClass("facet__cnt--show");

            // Close select facet boxes when custom select grouped with facets
            if ($(".facet--select").length) {
                if (!$(this).hasClass("facet--select")) {
                    app.closeAllSelect();
                }
            }
        });
        app.subFacetDropdown();
    };
    app.closeAllFacetDropdown = function (clicked) {
        $(".facet--dropdown").not(clicked).removeClass("facet--open").find(".facet__cnt").removeClass("facet__cnt--show");
        $(".sub-facet--expandown").removeClass("sub-facet--open").find(".sub-facet__cnt").removeClass("sub-facet__cnt--show").slideUp();
    };
    app.triggerCloseAllFacetDropdown = function () {
        /*if the user clicks anywhere outside the facet box,
        then close all facet boxes:*/
        $(document).on("click", app.closeAllFacetDropdown);

        // Don't close facet boxes when user clicks on facet content
        $(document).on("click", ".facet__cnt", function (event) {
            // its click event doesn't bubble up to the document
            event.stopPropagation();
        });
    };
    /**
     * End Facet Dropdown
     */

    /**
     * Scroll Effects
     */
    app.revertAllMobEffects = function () {
        if (app.winWidth > 767) {
            app.$body.removeClass("headMB-fx");

            app.header.$header.css({
                height: "unset"
            });

            if ($(".collection--landing .collection-header").length) {
                app.$body.removeClass("h-fx fg-fx");

                app.header.$middleBar.slideDown({
                    complete: function complete() {
                        app.header.middleBarH = app.header.$middleBar.outerHeight();
                    }
                });
                app.catLanding.$fixTitle.slideUp();
                app.catLanding.$defTitle.slideDown({
                    duration: "250"
                });

                app.catLanding.$facetGroup.css({
                    position: "relative",
                    top: "unset",
                    height: "unset"
                });
            }
        }
    };

    /**
     * Header
     */
    app.initHeaderScroll = function () {
        app.header.$header = $(".header");
        app.header.$middleBar = app.header.$header.find(".main-menu-bar");
        app.header.middleBarPos = app.findElPos(app.header.$middleBar);
        app.header.middleBarH = app.header.$middleBar.outerHeight();
        app.header.scroll();
        app.header.setHeaderHeight();
    };
    app.header = {
        scroll: function scroll() {
            if ($(window).outerWidth() < 768) {
                if (app.winPos >= app.header.middleBarPos) {
                    app.$body.addClass("headMB-fx");
                } else {
                    app.$body.removeClass("headMB-fx");
                }
            }
        },
        setHeaderHeight: function setHeaderHeight() {
            if ($(window).outerWidth() < 768) {
                app.header.middleBarH = app.header.$middleBar.outerHeight();
                app.header.headerH = 115;
                app.header.$header.css({
                    height: app.header.headerH + "px"
                });
            }
        }
    };
    /**
     * End Header
     */

    /**
     * Category Landing
     */
    app.initCategoryLanding = function () {
        if ($(".collection--landing .collection-header").length) {
            app.catLandingAnimating = false;

            app.catLanding.$heading = $(".collection--landing .collection-header");
            app.catLanding.$defTitle = app.catLanding.$heading.find(".title--default");
            app.catLanding.$fixTitle = app.catLanding.$heading.find(".title--fixed");
            app.catLanding.$facetGroup = app.catLanding.$heading.find(".js-facets");

            app.catLanding.headingH = app.catLanding.$heading.outerHeight();
            app.catLanding.fixTitleH = app.catLanding.$fixTitle.outerHeight();
            app.catLanding.defTitleH = app.catLanding.$defTitle.outerHeight();

            app.catLanding.headingPos = app.findElPos(app.catLanding.$defTitle) - app.header.$middleBar.outerHeight();
            app.catLanding.facetGroupPos = app.findElPos(app.catLanding.$facetGroup) - app.catLanding.defTitleH;

            app.catLanding.scroll();
        }
    };
    app.catLanding = {
        scroll: function scroll() {
            if ($(".collection--landing .collection-header").length) {
                if ($(window).outerWidth() < 768) {
                    //app.catLanding.headingPos = app.findElPos(app.catLanding.$heading);
                    if (app.winPos >= app.catLanding.headingPos) {
                        if (app.scrollUp == false) {
                            app.catLandingAnimating = true;
                            if (!app.$body.hasClass("h-fx")) {
                                app.$body.addClass("h-fx");
                                app.catLanding.$defTitle.slideUp({
                                    duration: "50",
                                    complete: function complete() {}
                                });
                            }
                            app.header.$middleBar.slideUp({
                                complete: function complete() {}
                            });
                            app.catLanding.$fixTitle.slideDown({
                                duration: "50",
                                complete: function complete() {
                                    app.catLanding.fixTitleH = app.catLanding.$fixTitle.outerHeight();
                                    app.catLanding.setHeadingHeight();
                                    setTimeout(function () {
                                        app.catLandingAnimating = false;
                                    }, 200);
                                }
                            });
                        } else {
                            app.catLanding.$fixTitle.slideUp();
                            app.header.$middleBar.slideDown({
                                duration: "50",
                                complete: function complete() {
                                    app.header.middleBarH = app.header.$middleBar.outerHeight();
                                    setTimeout(function () {
                                        app.catLandingAnimating = false;
                                    }, 200);
                                }
                            });
                        }
                    } else if (app.$body.hasClass("h-fx")) {
                        app.catLandingAnimating = true;
                        app.$body.removeClass("h-fx");

                        app.catLanding.revertHeadingHeight();

                        app.header.$middleBar.slideDown({
                            duration: "250",
                            complete: function complete() {
                                app.header.middleBarH = app.header.$middleBar.outerHeight();
                                setTimeout(function () {
                                    app.catLandingAnimating = false;
                                }, 200);
                            }
                        });
                        app.catLanding.$fixTitle.slideUp();
                        app.catLanding.$defTitle.slideDown({
                            duration: "250"
                        });
                    }
                    if (app.winPos + app.catLanding.fixTitleH >= app.catLanding.facetGroupPos) {
                        if (app.scrollUp == false) {
                            if (!app.$body.hasClass("fg-fx")) {
                                app.$body.addClass("fg-fx");
                            }

                            app.catLanding.$facetGroup.css({
                                position: "fixed",
                                top: app.catLanding.fixTitleH + "px"
                            });
                        } else {
                            app.catLanding.$facetGroup.css({
                                position: "fixed",
                                top: app.header.middleBarH + "px"
                            });
                        }
                    } else if (app.$body.hasClass("fg-fx")) {
                        app.$body.removeClass("fg-fx");

                        app.catLanding.$facetGroup.css({
                            position: "relative",
                            top: "unset"
                        });
                    }
                }
            }
        },
        setHeadingHeight: function setHeadingHeight() {
            if (app.catLanding.$heading.length) {
                app.catLanding.$heading.css({
                    height: app.catLanding.headingH - app.catLanding.defTitleH + "px"
                });
            }
        },
        revertHeadingHeight: function revertHeadingHeight() {
            if (app.catLanding.$heading.length) {
                app.catLanding.$heading.css({
                    height: "unset"
                });
            }
        }
    };
    /**
     * End Category Landing
     */
    /**
     * End Scroll Effects
     */

    /**
     * Blog
     */
    app.blog = {
        toggleTagsDropdown: function toggleTagsDropdown() {
            $("body").toggleClass("blog-tags-dropdown-on");
            $(".js-dropdown-blog-tags").slideToggle();
        },
        events: function events() {
            app.$body.on("click", ".js-trigger-blog-nav", function (event) {
                event.stopPropagation();
                event.preventDefault();
                app.blog.toggleTagsDropdown();
            });
            app.$document.on("click", "body.blog-tags-dropdown-on", function (event) {
                app.blog.toggleTagsDropdown();
            });
        },
        init: function init() {
            app.blog.events();
        }
    };

    /**
     * Cart
     */
    app.cart = {
        cache: function cache() {
            app.cart.$addToCartBtn = $(".js-add-to-cart");
            app.cart.$cartCount = $(".micro-cart .cart-count");

            // Mini Cart
            app.cart.$mini = {
                $container: $(".js-cart-mini"),
                $close: $(".js-close-mini-cart")
            };
        },
        /**
         * Cart
         */
        mini: function mini(opts) {
            var config = {
                ani_timer: 350,
                btn: ".js-mini-cart-increment-btn",
                cart_count: ".js-cart-count",
                count_div: ".js-cart-count",
                empty: "cart--mini--empty",
                hide_if_empty: ".js-hide-if-empty",
                id: "#miniCart",
                item: ".js-cart-mini-item",
                item_actions: ".js-cart-mini-item-actions",
                item_qty_none: "js-cart-mini-no-more-qty",
                list: "#miniCartItems",
                loading: "loading",
                inc_input: ".js-mini-cart-input",
                remove_btn: ".js-cart-mini-item-remove",
                totals: "#miniCartTotals",
                subtotal: "#miniCartSubtotal"
            },
                c = $.extend(config, opts),
                init = function init() {
                changeTrigger();
                removeTrigger();
            },

            // limit quantity field from updating more than items available
            limitQuantity = function limitQuantity(item, newQty) {
                var article_id = item.obj[0].id;
                var inc_input = $("#" + article_id).find(c.inc_input);
                var inc_input_value = inc_input.val();

                $.getJSON("/cart.js", function (cart) {
                    var items = cart.items;
                    var item_el = items.map(function (val) {
                        return {
                            variant_id: val.variant_id,
                            final_line_price: val.final_line_price,
                            final_price: val.price,
                            qty: val.quantity
                        };
                    });

                    function getData(id) {
                        var item;
                        item_el.forEach(function (elem) {
                            var el = elem.variant_id.toString();
                            if (el === id) {
                                item = elem;
                            }
                        });
                        return item;
                    }
                    var elem = getData(article_id);
                    if (elem) {
                        var this_item_count = elem.qty;
                        $("#" + article_id).find(".item__line-price").html("$" + (elem.final_line_price / 100).toFixed(2));
                        setTimeout(function () {
                            $("article#" + article_id).find(c.inc_input).prop("disabled", false);
                            $("article#" + article_id).removeClass("loading");
                        }, 250);
                    }
                    if (inc_input_value > this_item_count) {
                        inc_input.val(this_item_count);
                        $("." + c.item_qty_none).remove();
                        $("#" + article_id).find(c.item_actions).prepend('<div class="' + c.item_qty_none + ' item__no-more-qty">No more than ' + this_item_count + " items available.</div>");

                        setTimeout(function () {
                            $("." + c.item_qty_none).fadeOut();
                        }, 1500);
                    }
                });
            },

            // Set the quantity of an item already in the cart
            updateItemQty = function updateItemQty(item, newQty) {
                item.obj.addClass(c.loading);
                item.obj.find(c.inc_input).prop("disabled", true);
                $.ajax({
                    type: "POST",
                    url: "/cart/change.js",
                    cache: false,
                    data: {
                        quantity: newQty,
                        id: item.key
                    },
                    success: function success(data) {
                        $(c.cart_count).html(data.item_count);
                        app.cart.recalcSubTotal();
                        if (newQty === 0) {
                            item.obj.slideUp(c.ani_timer, function () {
                                $(this).remove();
                            });
                        }

                        if (data.item_count === 0) {
                            resetCart();
                        } else {
                            itemInCart();
                            limitQuantity(item, newQty);
                        }
                    },
                    dataType: "json"
                });
            },

            // Update item when quantity input changed or numbers keyup
            changeTrigger = function changeTrigger() {
                app.$document.on("focus", c.inc_input, function (event) {
                    $(this).on("mousewheel.disableScroll", function (event) {
                        event.preventDefault();
                    });
                });
                app.$document.on("blurr", c.inc_input, function (event) {
                    $(this).off("mousewheel.disableScroll");
                });
                app.$document.on("change", c.inc_input, function (event) {
                    checkChange(event);
                });
                app.$document.on("keyup", c.inc_input, function (event) {
                    var num = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
                    if (num.indexOf(event.key) >= 0) {
                        checkChange(event);
                    }
                });

                function checkChange(event) {
                    clearTimeout($.data(event.target, "changeCheck"));
                    $.data(event.target, "changeCheck", setTimeout(function () {
                        var item = {};
                        item.obj = $(event.target).parents(".js-cart-mini-item");
                        item.variant_id = item.obj.data("variant-id");
                        item.key = item.obj.data("key");
                        if (Number.isNaN(parseInt($(event.target).val()))) {
                            // NaN
                            $(event.target).val($(event.target).attr("min"));
                            app.cart.updateInputVal($(event.target));
                        }
                        updateItemQty(item, parseInt($(event.target).val()));
                    }, 250));
                }
            },

            // Remove line item
            removeTrigger = function removeTrigger() {
                $(c.id).on("click", c.remove_btn, function (e) {
                    e.preventDefault();

                    var item = {};
                    item.obj = $(this).parents(c.item);
                    item.variant_id = item.obj.data("variant-id");
                    item.key = item.obj.data("key");
                    updateItemQty(item, 0);
                });
            },
                itemInCart = function itemInCart() {
                //$(c.count_div).show();
                $(c.id).removeClass(c.empty);
            },
                resetCart = function resetCart() {
                $(c.count_div).text("0");
                $(c.id).addClass(c.empty);
            };

            return {
                init: init()
            };
        },
        // Update shipping
        updateShipping: function updateShipping(freeShippingState, spendMoreAmt) {
            if (freeShippingState == true) {
                // Free Shipping
                $(".js-shipping").text("You Qualify For Free Discreet Shipping!");
            } else {
                // Spend more for shipping
                $(".js-shipping").html('Spend <span class="red-highlight">$' + spendMoreAmt + "</span> More & Get Free Shipping");
            }
        },
        // Get cart subtotal
        recalcSubTotal: function recalcSubTotal() {
            var $subTotal = $("#miniCartSubtotal");

            $.getJSON("/cart.js", function (cart, textStatus) {
                var nt = (cart.total_price / 100).toFixed(2),
                    freeShippingState = void 0,
                    freeShippingAmt = parseInt($(".js-shipping").data("free-shipping-amt")).toFixed(2),
                    spendMoreAmt = void 0;
                $subTotal.html("$" + nt);
                if (parseInt(nt) >= parseInt(freeShippingAmt)) {
                    // Free Shipping
                    freeShippingState = true;
                } else {
                    // Spend more for shipping
                    freeShippingState = false;
                    spendMoreAmt = (freeShippingAmt - nt).toFixed(2);
                }
                app.cart.updateShipping(freeShippingState, spendMoreAmt);
            });
        },
        // Load cart items into mini cart
        refresh: function refresh(opts) {
            var config = {
                count: ".js-cart-count",
                miniCart: "#miniCart",
                empty: "cart--mini--empty",
                list: "#miniCartItems",
                item: ".js-cart-mini-item",
                itemTemplate: function itemTemplate(obj) {
                    var img = obj.image,
                        img_standard = img.replace(".jpg", "_50x50_crop_center.jpg"),
                        img_retina = img.replace(".jpg", "_150x150_crop_center.jpg"),
                        original_price = (obj.original_price / 100).toFixed(2),
                        sale_price = obj.total_discount,
                        variants = obj.variant_options;

                    if (sale_price !== 0) {
                        sale_price = ((obj.original_price - sale_price) / 100).toFixed(2);
                    }

                    var itemTitle = "{{ item.title | escape }}";
                    var variantsListItemHTML = (variants[0] === "Default Title" ? "" : '<li class="item__variant">' + variants[0] + "</li>") + (variants[1] ? '<li class="item__variant">' + variants[1] + "</li>" : "") + (variants[2] ? '<li class="item__variant">' + variants[2] + "</li>" : "");
                    var salePriceHTML = sale_price !== 0 ? '<span class="bag-item__sale-price">$' + sale_price + "</span>" : "";

                    var item = "<article id=\"" + obj.variant_id + "\" data-variant-id=\"" + obj.variant_id + "\" data-key=\"" + obj.key + "\"\n                         class=\"cart__item item js-cart-mini-item\">\n                            <div class=\"item__thumb\">\n                                <div class=\"item__img-wrapper\">\n                                    <img src=\"" + img_standard + "\" data-src=\"" + img_retina + "\" class=\"responsive-image__image lazyload item__img\" alt=\"" + itemTitle + "\" />\n                                </div>\n                            </div>\n                            <div class=\"item__cnt\">\n                                <div class=\"item__header\">\n                                    <h2 class=\"item__title\"><a class=\"item__link\" href=\"" + obj.url + "\" title=\"" + obj.product_title + "\">" + obj.product_title + "</a></h2>\n                                    <a href=\"/cart/change/" + obj.variant_id + "?quantity=0\" title=\"Remove item\" class=\"item__remove btn-remove js-cart-mini-item-remove\" role=\"button\" data-variant-id=\"" + obj.variant_id + "\">\n                                    <i class=\"icon-bin\"></i>\n                                    </a>\n                                </div>\n                                <ul class=\"item__variants\">\n                                    " + variantsListItemHTML + "\n                                </ul>\n                                <div class=\"item__footer\">\n                                    <div class=\"item__actions js-cart-mini-item-actions\">\n                                        <div class=\"item__qty increment\">\n                                            <label class=\"increment__label\">Qty</label>\n                                            <input type=\"hidden\" name=\"id\" value=\"28388651303011\" class=\"js-variant-id\">\n                                            <input min=\"1\" max=\"\" type=\"number\" id=\"quantity\" name=\"quantity\" value=\"" + obj.quantity + "\" size=\"3\" class=\"increment__field js-mini-cart-input js-quantity-field\">\n                                        </div>\n                                    </div>\n                                    <div class=\"item__cost-wrap\">\n                                        <p class=\"item__price-wrapper\"><span class=\"item__price\">$" + original_price + "</span>" + salePriceHTML + "</p>\n                                        <p class=\"item__line-price-wrapper\"><span class=\"item__line-price\">$" + (sale_price !== 0 ? (salePriceHTML * obj.quantity).toFixed(2) : (original_price * obj.quantity).toFixed(2)) + "</span></p>\n                                    </div>\n                                </div>\n                            </div>\n                        </article>";
                    return item;
                }
            },
                c = $.extend(config, opts),
                init = function init() {
                $.getJSON("/cart.js", function (cart, textStatus) {
                    var items = cart.items,
                        count = cart.item_count,
                        html = [];

                    for (var i in items) {
                        var item = c.itemTemplate(items[i]);
                        html.push(item);
                    }

                    $(c.list).find(c.item).remove();
                    $(c.list).prepend(html);

                    app.cart.recalcSubTotal();

                    if (count > 0) {
                        $(c.count).html(count).parent().show();
                    }
                });
            };

            return init();
        },
        // Update Input Value
        updateInputVal: function updateInputVal($targetEl) {
            var inputVal = $targetEl.val();
            $targetEl.attr("value", inputVal);
            // CART JS
            if ($targetEl.closest("form[data-cart-submit]").length) {
                var $addToCart;
                if (app.$body.hasClass("template-product")) {
                    $addToCart = $targetEl.closest("form[data-cart-submit]").find(app.cart.$addToCartBtn);
                } else {
                    $addToCart = $(".js-quick-shop").find(app.cart.$addToCartBtn);
                }
                // To add more than 1 quantity, you must specify a quantity to add with an optional data-cart-quantity attribute.
                $addToCart.attr("data-cart-quantity", inputVal);
            }
        },
        // Displaying the state in add to cart button while adding to cart
        addStateAddToCart: function addStateAddToCart() {
            if ($(".product__action--add-to-cart").length) {
                $(".product__action--add-to-cart").each(function () {
                    if (!$(this).find(".circle-loader").length) {
                        $(this).prepend('<div class="circle-loader"><div class="checkmark draw"></div><div class="crossmark draw"></div></div>');
                    }
                });
            }
        },
        // Actions when add to cart button is clicked
        addToCart: function addToCart($targetEl) {
            var variantId = void 0,
                qty = 1,
                $form = void 0,
                properties = {},
                isGiftCard = void 0;
            if ($targetEl.closest("form").length) {
                $form = $targetEl.closest("form");
                variantId = $form.find(".js-variant-id").val();
                qty = $form.find(".js-quantity-field").val();
                isGiftCard = $form.find(".js-is-gift").val();

                // Display loading info on add to cart btn
                $targetEl.find(".circle-loader").css({
                    display: "inline-block"
                });
                if (isGiftCard) {
                    var $lineItems = $form.find(".gift-card-line-items");
                    properties["Recipient Name"] = $lineItems.find(".rec_name").val();
                    properties["Recipient Email"] = $lineItems.find(".rec_email").val();
                    properties["Recipient Message"] = $lineItems.find(".rec_message").val();
                }
                // Call the addItem() method.
                // Note the empty object as the third argument, representing no line item properties.
                CartJS.addItem(variantId, qty, properties, {
                    // Define a success callback to display a success message.
                    success: function success(data, textStatus, jqXHR) {
                        // Adding add to cart success status class on body
                        app.$body.addClass("cart-success");

                        // Displaying success status on add to cart btn
                        $targetEl.find(".circle-loader").toggleClass("load-complete");
                        $targetEl.find(".checkmark").show();

                        // Removing previous error message
                        app.$atcState.removeClass("atc-state--error");

                        // Open Mini Cart
                        app.cart.openmini();

                        // Hide cart is empty message
                        $("#miniCart").removeClass("cart--mini--empty");

                        // Refresh Mini Cart Content
                        app.cart.refresh();

                        setTimeout(function () {
                            // Removing add to cart success status class on body
                            app.$body.removeClass("cart-success");

                            // Removing success status on add to cart btn
                            $targetEl.find(".circle-loader").hide();
                            $targetEl.find(".checkmark").hide();
                            $targetEl.find(".circle-loader").toggleClass("load-complete");
                        }, 2500);
                    },

                    // Define an error callback to display an error message.
                    error: function error(jqXHR, textStatus, errorThrown) {

                        // Adding add to cart error status class on body
                        app.$body.addClass("cart-error");

                        // Displaying error message while adding to cart
                        var descTitle = '"description":"',
                            descTitleLength = descTitle.length,
                            descTitleIndex = jqXHR.responseText.indexOf(descTitle);
                        var responseTextDesc = jqXHR.responseText.slice(descTitleIndex + descTitleLength, jqXHR.responseText.indexOf('"', descTitleIndex + descTitleLength)).replace(/[.*+?^${}()|[\]\\]/g, "");
                        // if (jqXHR.status === 422) {
                        //     responseTextDesc = "The variant you've selected is sold out. Please check the product page to see if there are other variants in stock.";
                        // }
                        app.$atcState.addClass("atc-state--error");
                        app.$atcCnt.html("<h2 class=\"atc-state__desc\">" + responseTextDesc + "</h2>");

                        // Displaying error status on add to cart btn
                        $targetEl.find(".circle-loader").toggleClass("load-error");
                        $targetEl.find(".crossmark").show();

                        setTimeout(function () {
                            // Removing add to cart error status class on body
                            app.$body.removeClass("cart-error");

                            // Removing error status on add to cart btn
                            $targetEl.find(".circle-loader").hide();
                            $targetEl.find(".crossmark").hide();
                            $targetEl.find(".circle-loader").toggleClass("load-error");
                        }, 2500);
                        setTimeout(function () {
                            // Removing error message
                            app.$atcState.removeClass("atc-state--error");
                        }, 50000);
                    }
                });
            }
        },
        // Open Mini Cart
        openmini: function openmini() {
            app.cart.$mini.$container.addClass("cart--mini--open");
            app.$body.addClass("mini-cart-open");
        },
        // Close mini cart
        closemini: function closemini() {
            app.cart.$mini.$container.removeClass("cart--mini--open");
            app.$body.removeClass("mini-cart-open");
        },
        events: function events() {
            // Trigger Update Input Value
            app.$document.on("click", "input[type=number]", function (e) {
                app.cart.updateInputVal($(this));
            });

            // Add to cart button click
            app.$body.unbind("click").on("click", ".js-add-to-cart", function (event) {
                event.preventDefault();
                app.cart.addToCart($(event.target));
            });
            // Message about State of Add to Cart closed
            app.$body.on("click", ".atc-state__close", function () {
                app.$atcState.removeClass("atc-state--error");
            });
            /**
             * Triggered whenever Cart.js completes processing the current request queue.
             *
             * Note that if you make multiple Ajax-triggered calls to Cart.js (say, two calls to addItem in a row), this event will still * only fire once at the end after processing all requests.
             */
            app.$document.on("cart.requestComplete", function (event, cart) {
                app.cart.$cartCount.html(cart.item_count); //   update an element with the number of items in the cart after a request
            });

            // Trigger Open mini cart
            app.$body.on("click", ".js-open-mini-cart", function () {
                app.cart.openmini();
            });

            app.$body.on("click", "#miniCart, .js-open-mini-cart", function (event) {
                if (!$(event.target).hasClass("js-close-mini-cart")) {
                    // its click event doesn't bubble up to the document
                    event.stopPropagation();
                }
            });
            // Trigger close mini cart
            // $('body.mini-cart-open').click(function() {
            //     app.cart.closemini();
            // });
            app.$document.on("click", ".js-close-mini-cart, body.mini-cart-open", function () {
                app.cart.closemini();
            });
        },
        init: function init() {
            app.cart.cache();
            // Add elements to display add to cart status
            app.cart.addStateAddToCart();

            // All events related to cart
            app.cart.events();
        }
    };
    /**
     * End Cart
     */

    /**
     * Main Events Functions
     */
    app.load = function () {};
    app.scroll = function () {
        app.winPos = app.$window.scrollTop();
        app.currentScrollTop = app.winPos;

        if (app.lastScrollTop < app.currentScrollTop) {
            app.scrollUp = false;
        } else if (app.lastScrollTop > app.currentScrollTop) {
            app.scrollUp = true;
        }
        app.lastScrollTop = app.currentScrollTop;

        app.header.scroll();

        if (app.catLandingAnimating == false) {
            app.catLanding.scroll();
        }
    };
    app.resize = function () {
        app.winPos = app.$window.scrollTop();

        app.winWidth = $(window).outerWidth();

        app.header.scroll();
        app.header.setHeaderHeight();

        app.catLanding.scroll();

        app.revertAllMobEffects();
    };
    /**
     * End Main Events Funcitons
     */

    $(document).ready(function () {
        app.init();

        $(window).scroll(function () {
            app.scroll();
        });

        $(window).resize(function () {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(app.resize, 250);
        });
    });

    $(window).on("load", function () {
        app.load();
    });
}(window, document, jQuery);
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

jQuery(function ($) {

    /* Placeholder JS */
    /*==========================*/
    var test = document.createElement('input');
    if (!('placeholder' in test)) {
        $('[placeholder]').each(function () {
            if ($(this).val() === '') {
                var hint = $(this).attr('placeholder');
                $(this).val(hint).addClass('hint');
            }
        });
        $('[placeholder]').focus(function () {
            if ($(this).val() === $(this).attr('placeholder')) {
                $(this).val('').removeClass('hint');
            }
        }).blur(function () {
            if ($(this).val() === '') {
                $(this).val($(this).attr('placeholder')).addClass('hint');
            }
        });
    }

    /* Form validation JS */
    /*==========================*/

    $('input.error, textarea.error').focus(function () {
        $(this).removeClass('error');
    });

    $('form :submit').click(function () {
        $(this).parents('form').find('input.hint, textarea.hint').each(function () {
            $(this).val('').removeClass('hint');
        });
        return true;
    });

    /* Remove SVG images to avoid broken images in all browsers that don't support SVG. */
    /*==========================*/

    var supportsSVG = function supportsSVG() {
        return document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#Image', '1.1');
    };
    if (!supportsSVG()) {
        $('img[src*=".svg"]').remove();
    }

    /* Prepare to have floated images fill the width of the design on blog pages on small devices. */
    /*==========================*/

    var images = $('.article img').on('load', function () {
        var srcAttr = $(this).attr('src');
        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if ((typeof srcAttr === 'undefined' ? 'undefined' : _typeof(srcAttr)) !== (typeof undefined === 'undefined' ? 'undefined' : _typeof(undefined)) && srcAttr !== false) {
            var src = $(this).attr('src').replace(/_grande\.|_large\.|_medium\.|_small\./, '.');
            var width = $(this).width();
            $(this).attr('src', src).attr('width', width).removeAttr('height');
        }
    });
});
