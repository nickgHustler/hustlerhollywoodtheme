
if (window.template === "account") {
  // REDORDER FUNCTIONALITY:
  $('.reorder-btn').click(function() {
    const orderObj = {}
    $(this).siblings('.order-item').each(function() {
      if (!orderObj[$(this).attr("data-index")]) orderObj[$(this).attr("data-index")] = {};
      if ($(this).hasClass("varId")) {
        orderObj[$(this).attr("data-index")].variantId = $(this).val();
      } else if ($(this).hasClass("quantity")) {
        orderObj[$(this).attr("data-index")].quantity = $(this).val();
      } else if ($(this).hasClass("property")) {
        if (!orderObj[$(this).attr("data-index")].properties) orderObj[$(this).attr("data-index")].properties = {};
        orderObj[$(this).attr("data-index")].properties[$(this).attr("data-key")] = $(this).val();
      }
    })
      const items = Array.from(Object.values(orderObj)).map(function(item) {
        return {
            quantity: item.quantity,
            id: item.variantId,
            properties: item.properties ? item.properties : {}
        }
      })
      $.post('/cart/add.js', {
          items
        }).then(res => {
            // Adding add to cart success status class on body
            app.$body.addClass("cart-success");

            // Removing previous error message
            app.$atcState.removeClass("atc-state--error");

            // Open Mini Cart
            app.cart.openmini();

            // Hide cart is empty message
            $("#miniCart").removeClass("cart--mini--empty");

            // Refresh Mini Cart Content
            app.cart.refresh();

            setTimeout(function () {
                // Removing add to cart success status class on body
                app.$body.removeClass("cart-success");
            }, 2500);
        }).catch(err => {
            // Adding add to cart error status class on body
            app.$body.addClass("cart-error");
            const responseTextDesc = "Something went wrong. Please check the items are in stock and try again.";
            app.$atcState.addClass("atc-state--error");
            app.$atcCnt.html("<h2 class=\"atc-state__desc\">" + responseTextDesc + "</h2>");


            setTimeout(function () {
                // Removing add to cart error status class on body
                app.$body.removeClass("cart-error");
            }, 2500);

            setTimeout(function () {
                // Removing error message
                app.$atcState.removeClass("atc-state--error");
            }, 50000);
        });
  })
  // ANNEX REWARDS:
  const cust_email = window.customer_email;
  const cust_id =window.customer_id;
  function loadRewards(email) {
    fetch('https://hustler.conspireagency.com/loyalty/api/rewards', { // https://hustler.conspireagency.com/loyalty
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email
      })
    })
      .then(res => res.json())
      .then(res => {
        let rewardHTML = '';
        if (res.data) {
            const rewardInfo = res.data.rewards;
            if (rewardInfo && rewardInfo.length) {
                const sortedRewardInfo = rewardInfo.sort((a,b) => {
                  return Number(a.earned_points_required) - Number(b.earned_points_required)
                })
                let rewards = ''
                for (let i = 0; i < sortedRewardInfo.length; i++) {
                  let reward = sortedRewardInfo[i];
                  rewards += `
                    <option value=${i}>${reward.reward_name} - ${reward.earned_points_required} points</option>
                  `
                }
                rewardHTML = `
                  <div class="annex-element">
                    <div class="annex-top-bar">
                      <p class="rewards-txt">Redeem your rewards here!</p>
                      <p><a href="/pages/my-rewards">Rewards Dashboard</a></p>
                    </div>
                    <div class="field field--show-floating-label reward-field">
                      <div class="field__input-wrapper field__input-wrapper--select">
                        <select placeholder="Reward" autocomplete="shipping country" class="account-rewards-select" id="reward">
                          <option disabled>Reward</option>
                          ${rewards}
                        </select>
                      </div>
                    </div>
                    <div class="redeem-container">
                      <p>${res.data.points.replace('.00', '')} points remaining</p>
                      <div class="reward-redeem">
                        <button
                          class="button btn--square btn--accent reward-btn"
                        >
                          Redeem
                        </button>
                      </div>
                    </div>
                  </div>
                `
            } else {
            rewardHTML = `
              <div class="annex-element">
                <p style="text-align:center; font-weight:700">No available rewards found. Learn more about our <a style="text-decoration:underline;" href="/pages/my-rewards">Rewards Program</a>!</p>
              </div>
            `
            }
        } else {
          rewardHTML = `
              <div class="annex-element">
                <p style="text-align:center; font-weight:700">No available rewards found. Learn more about our <a style="text-decoration:underline;" href="/pages/my-rewards">Rewards Program</a>!</p>
              </div>
            `
        }
        $('#annex-root').append(rewardHTML);
        if (res.data) {
          if (res.data.rewards && res.data.rewards.length) {
            const rewardBtn = document.querySelector(".reward-btn");
            const sortedRewards = res.data.rewards.sort((a,b) => {
              return Number(a.earned_points_required) - Number(b.earned_points_required)
            })
            rewardBtn.addEventListener('click', (e) => {
              rewardBtn.classList.add("btn--disabled");
              return redeemReward(sortedRewards, $('#reward').val());
            });
          }
        }
      })
  }
  function redeemReward(rewards, i) {
    $('.reward-btn').attr('disabled', 'true')
    if (rewards[i].eligible === "NO") return;
    fetch('https://hustler.conspireagency.com/loyalty/api/redeem', {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        cust_email,
        cust_id,
        reward: rewards[i]
      })
    })
      .then(res => res.json())
      .then(res => {
        if (Number(res.error_code)) {
          $("#annex-error-message").addClass("annex-state--active")
          $("#annex-error-message").addClass("annex-state--error")
          $("#annex-error-message").removeClass("annex-state--success");
          $("#annex-error-message .annex-state__desc").html(res.description);
          $('#reward').removeClass("btn--disabled");
          window.scroll(0,0);
          setTimeout(function () {
              // Removing error message
              $("#annex-error-message").removeClass("annex-state--active");
              $("#annex-error-message").removeClass("annex-state--error");
          }, 50000);
        } else {
          $("#annex-error-message").addClass("annex-state--active")
          $("#annex-error-message").addClass("annex-state--success");
          $("#annex-error-message").removeClass("annex-state--error");
          $("#annex-error-message .annex-state__desc").html("Reward successfully redeemed. Check your email for the reward code!");
          setTimeout(function () {
              // Removing error message
              $("#annex-error-message").removeClass("annex-state--active");
              $("#annex-error-message").removeClass("annex-state--success");
          }, 50000);
          $('#annex-root').empty();
          loadRewards(cust_email);
        }
      })
  }
  function closeError() {
    $("#annex-error-message").removeClass("annex-state--active");
    $("#annex-error-message").removeClass("annex-state--error");
    $("#annex-error-message").removeClass("annex-state--success");
  }
  $(".annex-state__close").click(closeError);
  if (window.customer_email) {
   $(document).ready(function(){
        loadRewards(cust_email)
    });
 } else {
    $(document).ready(function(){
      const annexElement = `
        <div class="annex-element">
          <div class="annex-container">
            <p>No available rewards found. Login to redeem your points for rewards!</p>
            <a class="field__input-btn btn" style="padding: 1em;height: 3.5em;" href="/account/login">
              Login
            </a>
          </div>
        </div>
      `
        $('#annex-root').append(annexElement);
    });
 }
} else if (window.template === "login") {
  const params = new URLSearchParams(window.location.search);
  if (params.get("m") === "register") {
    $(".atc-state .atc-state__cnt").html("<h2 class=\"atc-state__desc\">You've successfully registered! We've sent you an email, please check your inbox to activate your account.</h2>");
    $("section.atc-state").addClass("atc-state--active");
    $("section.atc-state").addClass("atc-state--success");
    setTimeout(function () {
        // Removing error message
        $("section.atc-state").removeClass("atc-state--active");
        $("section.atc-state").removeClass("atc-state--success");
    }, 50000);
  }
} else if (window.template === "register") {
    var hasTriedAjax = false;
    $('#create_customer').submit(function(event) {
      if (hasTriedAjax) return;
      event.preventDefault();
      var data = $(this).serialize();

     //create new account
      $.post('/account', data)
        .done(function(res){
        var logErrors = $(res).find('.errors').text();

        //if there are errors show them in the html form
        if (logErrors != "" && logErrors != 'undefined'){
            $('#create_customer .errors').html(logErrors);
            $('#create_customer .errors').show();

        //if account creation is successful show checkout page
        }else{
          document.location.href = '/account/login?m=register';
        }
        }).fail(function(err){
          hasTriedAjax = true;
          $('#create_customer').submit();
        });
       return false;
    });
}